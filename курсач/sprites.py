import pygame


class Sprites(object):

    sprite_sheet = None

    def __init__(self, file_name):

        self.sprite_sheet = pygame.image.load(file_name).convert()

    def get_image(self, x, y, width, height):

        image = pygame.Surface([width, height]).convert()

        image.blit(self.sprite_sheet, (0, 0), (x, y, width, height))

        color_switch = image.get_at((0, 0))
        image.set_colorkey(color_switch)

        return image

    def get_image_part(self):

        character = pygame.Surface((80, 70), pygame.SRCALPHA)
        character.blit(self.sprite_sheet, (-14, -171))

        stage = pygame.Surface((350, 150), pygame.SRCALPHA)
        stage.blit(character, (130, 0))
        self.test_frames.append(stage)

    def image_at(self, rectangle, colorkey=None):

        rect = pygame.Rect(rectangle)
        image = pygame.Surface(rect.size).convert()
        image.blit(self.sprite_sheet, (0, 0), rect)
        if colorkey is not None:
            if colorkey == -1:
                colorkey = image.get_at((0, 0))
            image.set_colorkey(colorkey, pygame.RLEACCEL)
        return image

    def images_at(self, rects, colorkey=None):

        return [self.image_at(rect, colorkey) for rect in rects]

    def load_strip(self, rect, image_count, colorkey=None):

        tups = [(rect[0] + rect[2] * x, rect[1], rect[2], rect[3])
            for x in range(image_count)]
        return self.images_at(tups, colorkey)
