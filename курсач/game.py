import pygame


class Game(object):

    def __init__(self):

        self.entities = pygame.sprite.Group()
        self.backgrounds = pygame.sprite.Group()
        self.playerentity = pygame.sprite.Group()
        self.projectilegroup = pygame.sprite.Group()
        self.enemygroup = pygame.sprite.Group()
        self.enemygroup2 = pygame.sprite.Group()
        self.harmful_objects = pygame.sprite.Group()
        self.lives = pygame.sprite.Group()
        self.exitgroup = pygame.sprite.Group()
        self.menugroup = pygame.sprite.Group()
        self.titlegroup = pygame.sprite.Group()
        self.detectablegroup = pygame.sprite.Group()
        self.itemgroup = pygame.sprite.Group()

        self.camera = ""
        self.camerafocus = ""

        self.platforms = []

        self.screenfocus = "Title"
