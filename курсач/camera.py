import pygame
import constants


class Camera(object):
    def __init__(self, camera_function, width, height):
        self.camera_function = camera_function
        self.state = pygame.Rect(0, 0, width, height)

    def apply(self, target):
        return target.rect.move(self.state.topleft)

    def update(self, target):
        self.state = self.camera_function(self.state, target.rect)


def simple_camera(camera, target_rect):
    left, top, _, _ = target_rect
    _, _, width, height = camera
    return pygame.Rect(constants.HALF_WIDTH - left, constants.HALF_HEIGHT - top, width, height)


def complex_camera(camera, target_rect):
    left, top, _, _ = target_rect
    _, _, width, height = camera
    left, top, _, _ = -left + constants.HALF_WIDTH, -top + constants.HALF_HEIGHT, width, height

    left = min(0, left)
    left = max(-(camera.width-constants.SCREEN_WIDTH), left)
    top = max(-(camera.height-constants.SCREEN_HEIGHT), top)
    top = min(0, top)

    return pygame.Rect(left, top, width, height)
